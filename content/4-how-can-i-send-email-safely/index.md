# How can I send email safely?

![](images/1.png)

## How can I send email safely?

*Time to complete: 10 minutes*

---

In this lesson you will learn:

* How your email can be monitored.
* How you can use email more safely.
* How you can maintain a measure of anonymity with email.

---

Email communication uses the same principles as general Internet browsing, except that each message of ours has a specific destination: another person who also connects to the Internet through his or her country’s local provider. 1
1Frontline Defenders, www.frontlinedefenders.org/manuals/protection

---

![](images/2.png)

## How an email can be intercepted or monitored

Your email message can be intercepted at all major routing points on its way. If you live in a country with strong legal protection of privacy, the legislation will not apply when your email reaches the ISP of the recipient in a country with different privacy laws. While your email is in transit from country A to country B, it could pass the routers of several other countries.

---

![](images/3.png)

## Use HTTPS instead of HTTP
Use email that supports HTTPS secure connections. Always be sure you have a secure connection. When connecting to email services, do not connect via HTTP. Connect only via HTTPS. This to the default option in Gmail, which comes standard on most Android devices. Make sure anyone you communicate with is using HTTPS for their email.

---

![](images/4.png)

### Do not open attachments

Use the “view” option instead. Attachments may be used to distribute viruses and compromise the security of your computer or mobile device.

Always be sure you are communicating with the person you think you are. According to Frontline Defenders, “It is relatively easy to ‘spoof’ an email address. Spoofing means faking the name and email address of the sender to make it appear as a random address or that of someone you know.”

Always check return email addresses closely, to ensure the email comes from the correct address, mimicking email addresses is a common tool for sending malware and compromising computers and mobile devices.

For additional anonymity, communicate only between new, anonymous email accounts.

---

![](images/5.png)

### Use Alternate Browsers

Use a different browser for anonymous use and use it only for that. Reset the browser before and after each use (be certain to erase browsing history and remove cookies) You can also use a “private browsing” or “incognito” mode, which will not collect cookies and delete browsing history upon closing.

For added security, connect from a different IP address when doing things anonymously. For example, use your regular accounts when at home but use your anonymous accounts only when using wireless at a cafe. Remember using the same cafe repeatedly also reduces the chance of anonymity.

---

The only way to send email that is truly private and secure is with an encrypted email provider or an email client that enables device-side encryption. Use a device-side encryption if at all possible, and use an encryption option such as PGP that provides an open standard to ensure the encryption does what it claims.

The Electronic Frontier Foundation’s Surveillance Self Defense project provides a great introduction and overview for using encryption:

https://ssd.eff.org/en/module/what-encryption

---

## THINGS TO REMEMBER

* Only use email with HTTPS by default.
* HTTPS assists your email to be secure, but does not ensure your email is secure.
* Only open attachments after you verify the identity of the sender.
* Never use your personal email on a mobile device or computer that is not yours.
* Encrypting your email is the only way to ensure it will not be read by someone other than the receiver.

---

## QUIZ

#### 1. **Which of the following will help me read my email safely?**

* A. Always use HTTP
* B. Share an email account and communicate only via “drafts”
* C. Use an anonymous email account
* D. Always use HTTPS

#### 2. **Where can my email be intercepted or read?**

* A. On my own computer
* B. From my ISP
* C. From the email provider
* D. On the recipient’s computer

#### 3. **What is it called when someone impersonates your email address or one of your contacts?**

* A. Spotting
* B. Lying
* C. Spoofing
* D. Hacking

---

### ANSWERS

1. B, C, D
2. B, C, D
3. C