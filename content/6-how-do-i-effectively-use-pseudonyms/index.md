# How do I effectively use pseudonyms?

![](images/1.png)

## How do I effectively use pseudonyms?

*Time to complete: 5 minutes*

---

In this lesson you will learn:

* How pseudonyms protect your identity.
* How timestamps can reveal your identity.
* How to maintain your pseudonym online.

---

If you or a source are threatened, you must take additional steps to protect the identity of the threatened individual. At-risk journalists and threatened sources often use pseudonyms to protect their true identities. When you communicate digitally, you can leave clues to your true identity.

---

![](images/2.png)

Those who maintain (or contribute to) a blog or an Internet forum need to be aware that their anonymity will not be guaranteed merely by signing with a pseudonym. Every blog entry records the IP address (http://en.wikipedia.org/wiki/IP_address) of the computer it was sent from, and many ISPs (http://en.wikipedia.org/wiki/ISP) record all traffic that has passed through them. Therefore, if you are publishing sensitive information on a website, you must take precautions to not be found out. We discussed disguising your browsing in Lesson X.X. with Tor. If you must maintain anonymity online, be sure to use the Tor anonymity network to access your site and publish your content.

---

![](images/3.png)

## Timestamps

If you publish media to a website about government activity in your country by using the Tor network, the authorities will not see that you were sending this information to a particular blog, but they might notice that whenever you enter an Internet café, a new post appears on the site. This can be linked to you, especially if your Internet café owner writes down your name and the time of your visit. To overcome this difficulty, ask a colleague from another country to post the blog for you. Use a secure webmail service (or encryption) to send the article to your colleague and ask him/her to wait for a while before publishing it on your blog.

Your identity can be determined not only by the name you choose for yourself or your source. Your identity can also be determined by the trail you leave through your online activity.

---

## THINGS TO REMEMBER

* Be sure to delete your browsing history.
* Use an incognito window in Chrome or “private browsing” in Firefox.
* Be sure to disable cookies.
* Have a colleague who is not threatened publish your media.
* Set your media to publish automatically at a randomly assigned delay.
* Install HTTPS Everywhere on Firefox or Chrome.
* Use an anonymity service whenever you access the Internet.

---

## QUIZ

#### 1. **Why should I use a pseudonym?**

* A. Because its cooler than my real name.
* B. To protect the identity of myself or my source.
* C. Flash Gordon.
* D. Because my name is already taken on Facebook.

#### 2. **How can I prevent digital communications trails from revealing my true identity?**

* A. By always using incognito windows.
* B. By asking colleagues to publish media when I am not online.
* C. By using Tor to connect to the Internet.
* D. By using two email addresses.

#### 3. **If I use a pseudonym my identity will be**

* A. Completely protected.
* B. Whatever I tell people it is.
* C. Only protected if I prevent my communications trails from revealing my identity or location.
* D. Protected against cursory review.

---

### ANSWERS
---
1. B
2. A,B,C,D
3. C, D
