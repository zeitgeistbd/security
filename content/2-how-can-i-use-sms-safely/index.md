# How can I send and receive SMS safely?

![](images/1.png)

*Time to complete: 10 minutes*

---

In this lesson you will learn:

* The risks posed by communicating via SMS
* How SMS messages are monitored
* Some techniques to use when you suspect your SMS may be monitored

---

## How an SMS can be intercepted

![](images/2.png)

SMS is a highly convenient method of sending a message, but is not “secure.” SMS messages are saved by the service provider and may be intercepted by those with access to the cell tower transmitting the message. Your SMS messages can be accessed from your phone, or by anyone with access to the provider’s records or records stored at the cell tower used for transmission.

---

## How to avoid having your SMS be intercepted

![](images/3.png)

SMS is searchable and indexable. Unencrypted SMS is easy to monitor. If you or your source may be threatened, you should send only encrypted SMS; send them minimally and only between anonymous phones.

To send encrypted SMS, you must use a specific app. Both you and your source must use the same app. Send your source a message via your app of choice in order to initiate a key exchange. Once you’ve exchanged keys, you may communicate with your source via encrypted texts.

---

#### REMEMBER: You must use the encrypted text app in order to send encrypted texts. Your standard messaging app will not send encrypted texts.

---

![](images/4.png)

Before you communicate with a source by SMS, determine whether they have access to an encrypted SMS application. Also make sure they are aware that their SMS can be read by authorities and tied to them if they are not using an anonymous phone.

Do not transmit sensitive information via unencrypted SMS unless you are willing to have it read by the authorities. All SMS messages may be recorded by the service provider and may be read by the authorities.

---

![](images/5.png)

A simple method to avoid security breaches is to create a coded communication system with trusted colleagues. Build up a collection of codes to relay basic information. For example, “X” could mean you’re in danger, and “Z” could mean you’re safe. “Tree” could mean you want to meet up. “Banner” could be a cafe where you can meet.

It is up to you to figure out a system that is easy for you and your sources to remember.

There are third-party software solutions for secure texting, but they are not as easy to control as a personal SMS code.

---

## Delete Read Messages

![](images/6.png)

You should always delete SMS from your phone’s sent folder. There is nothing worse than creating an indexed archive of information that is waiting for the authorities to review in the event you are detained.

---

StoryMaker recommends using Open Whisper System’s encrypted texting app, [**Signal**](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=en).

* [https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=en](https://play.google.com/store/apps/details?id=org.thoughtcrime.securesms&hl=en)

---

## THINGS TO REMEMBER

* All SMS messages may be recorded by the service provider and may be read by the authorities.

* If you or your source is threatened and you must communicate via SMS, you should both use an encrypted SMS application at all times.

* Whether you have access to an encrypted SMS application or not, always communicate with anonymous phones that cannot be tied to your personal identity by any means, such as billing or registration information.

* Be sure to use a system of codes if you or your source do not have access to an encrypted SMS application and SMS is your only form of communication with your source.

* Never keep a record of your SMS messages on your phone. Your SMS messages can still be read if the authorities review your service provider’s logs.

---

# QUIZ

#### 1. **How can I prevent my SMS messages from being read?**

* A. Delete all SMS messages from my phone.
* B. Ensure the receiver deletes the SMS message after reading.
* C. Use an encrypted SMS application.
* D. Use an anonymous phone.


#### 2. **How can I communicate by SMS anonymously?**

* A. Use an encrypted SMS application.
* B. Use an anonymous phone.
* C. Delete all SMS messages from my phone.
* D. Ensure the receiver deletes the SMS message after reading.


#### 3. **How can I protect the information I send via SMS if I cannot encrypt the message?**

* A. Use an anonymous phone.
* B. Delete all SMS messages from my phone.
* C. Ensure the receiver deletes the SMS message after reading.
* D. Use a code previously arranged in person with the recipient of your message.

---

### ANSWERS

1. C
2. A, B
3. D