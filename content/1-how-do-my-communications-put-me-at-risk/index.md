# How do my communications put me at risk?

![](images/1.png)

#### How do digital communications put me at risk?

*Time to complete: 10 minutes*

---

In this lesson you will learn:

* What types of communication can be monitored.
* How mobile communications are monitored.
* How to avoid being monitored when possible.

---

Being an effective journalist involves using a variety of communication methods. Standard communication methods leave a trail that can be used by those who threaten you or your sources. In the process of producing a story, you may leave a digital trail that assists those who threaten you or your sources.

Understanding your communications trail is the first step to ensure that those who threaten you cannot take advantage of you or your sources.

---

![](images/2.png)

All standard methods of electronic communication leave trails. Electronic communication consists of a broad variety of methods:

* Phone calls
* Text messages (SMS / MMS)
* Email
* Voice over IP(VoIP) such as Skype or Google Hangout
* Web browsing
* Social network activity

---

All electronic communications rely on a similar framework. Communication is initiated by one individual, routes through a network, and is received by a second individual.

The nature of the threat posed by the communication depends on the system being used.

---

![](images/3.png)

### Monitoring cellular communications

Mobile phone network communication relies on a passive system where the user’s device is, by default, connected to the network.

---

![](images/4.png)

### Monitoring email at the ISP and national gateway

Web browsing, email, VoIP, and social network communication have traditionally relied on the user to actively connect to the network before communication may occur.

---

![](images/5.png)

As more users rely on their mobile for all data and communication services, all of these communications begin to rely on passive communication. Although you must take an action to communicate with a social network or receive email on a computer, your phone will receive these communications by default, once initiated.

---

![](images/6.png)

See this list of tools for maintaining privacy and anonymity when engaged in electronic communication. Later lessons will explain best practices for each type of communication.

### Suggested tools:

* SMS: Signal
* VoIP/Calls: Signal
* Anonymous communication: Orbot
* Browsing: Orfox
* Chat: Zom Mobile Messenger
* Email: Protonmail
* Notes: NoteCipher
* Passwords: KeePassDroid

---

### How does my mobile phone put me at risk?

![](images/8.jpg)

Both your SIM card and your mobile phone itself can identify who you are. Any communication your phone has with the mobile network — whether placing or receiving a call, sending a message, browsing the web, or just remaining connected — includes identifying information about the phone and the SIM card.

---

![](images/9.jpg)

#### How can a SIM card compromise me?

The Subscriber Identity Module (SIM) card stores information about the mobile service subscriber. It is what your service provider uses to identify who you are, based on personal information or documents that you submitted.

---

#### How can my mobile phone compromise me?

![](images/10.jpg)

A SIM card identifies you, but your mobile phone can get you into trouble. Each mobile phone has a unique number, the International Mobile Equipment Identity (IMEI). This number can be tracked at all times.

Check your mobile phone for the IMEI (often located behind the battery) or dial `*#06#` from your phone to find out your mobile phone’s unique number.

---

You may assume your SIM card is the only way for a mobile service provider or authorities to identify you or track your location. Many people change their SIM cards frequently but continue to use the same phone. This is a mistake! The combination of your SIM card (which identifies you) and IMEI number (which identifies your phone) allows you to be identified and tracked constantly.

Your mobile phone constantly registers your location on every cell tower it connects to. Take out your battery or leave your phone at home if you do not want to be tracked.

---

![](images/11.png)

## How does my phone show my location?

When your mobile phone is switched on, the network knows your location, triangulated from the cell towers nearby that record your phone’s signal. Your location might be accurate within a few meters in a densely populated area but only to a few hundred meters in a rural area with few cell towers. If you make or receive a call or text message, your location at that time is stored in network records. Note that this is a function of the mobile network, not surveillance. All networks triangulate your signal. This is important to remember as this information can be used against you!

---

![](images/12.png)

## Can my phone be used to eavesdrop on me?

Even when you are not using your mobile phone, it can be used as a secret microphone for others to listen to you. Take out your battery (do not just turn the phone off) or keep your phone far away while having private conversations.

In normal use, your text messages and phone calls can be monitored by your network operator. Calls can be listened to and recorded, and recordings can be passed (legally or illegally) to someone else.

---

A few steps you can take to stay safer:

* Buy prepaid SIM cards, and if at all possible avoid registering the SIM in your name. Use cash. Credit/debit transactions can link you to the SIM card.
* Buy a cheap, low-tech mobile phone that you do not mind throwing away if necessary.
* If you are concerned about being monitored, do not just change your SIM card; get rid of your mobile phone too.
* Always try to avoid sharing sensitive information over your mobile phone.

---

## THINGS TO REMEMBER

* Your SIM card and your mobile phone can identify you.
* Your mobile phone can track you.
* Your mobile phone can eavesdrop on you.
* Carry extra SIM cards from other mobile network operators that you can switch with your regular SIM. If possible, carry more than one phone.
* Take your battery out of your phone or leave it at home when travelling to and from group meetings to avoid cell phone triangulation and location tracking.
* If you are concerned that you are being tracked by authorities, get rid of both the SIM card and the phone that you have been using.
* Airplane mode is not a failsafe option to avoid tracking; your phone may still enable the manufacturer to access it via a backdoor.

---

The content for this lesson is primarily taken from content produced by Mobile Active’s now-closed Safer Mobile project and the Tibet Action Institute http://tibetaction.net.

---

## QUIZ

#### 1. **What communications leave trails?**

* A. Email + SMS
* B. Phone calls
* C. Handwritten notes
* D. Web browsing

#### 2. **How do mobile phones connect to the network by default?**

* A. By dialing phone numbers.
* B. Only when making a phone call.
* C. Mobile phones engage in passive communication constantly.
* D. Only when making a phone call or sending an SMS.

#### 3. **What is the best way to avoid being tracked by my mobile phone?**

* A. Change my SIM card.
* B. Get rid of my mobile phone.
* C. Take out my battery.
* D. Buy prepaid SIM cards.

#### 4. **What can I do to prevent my phone from eavesdropping on me?**

* A. Change my SIM card.
* B. Turn my phone off.
* C. Put me phone on airplane mode.
* D. Take out my battery.

#### 5. **How is my phone identified?**

* A. By the SIM card registration
* B. By the phone number
* C. By the IMEI registration
* D. By cellular towers

---

### ANSWERS

1. A, B, D
2. C
3. B
4. D
5. A, B, C, D