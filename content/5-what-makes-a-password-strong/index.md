# What Makes a Password Strong?

![](images/1.png)

*Time to complete: 10 minutes*

---

In this lesson you will learn:

* How passwords are cracked.
* How to make a strong password.
* How many passwords you should use.

---

Your password is often the first and most important guarantee of your information security. It is like a door to the house where you live. Using a bad password or no password at all is like leaving the door open all night. Maybe no one will walk in, or maybe someone will steal all your belongings. Pay great attention to how you create your passwords and where you keep them.


## How are passwords cracked?

![](images/2.png)

How do passwords get compromised? The authorities might observe you typing your password from a distance. They might install a spyware program that records all keystrokes typed into a computer and transmits them to the observer. Observe your surroundings carefully and update anti-spyware and anti-virus software frequently.

---

## How do I make a strong password?

There exist various methods for creating passwords that are difficult to break and easy to remember. A popular one is mnemonics (a method or system for improving the memory, such as a rhyme or acronym).

Lets take a common phrase:

* **To be or not to be? That is the question.**
  * *Hamlet, Shakespeare*

We can convert this to:

* **2Bon2B?TItQ**

In the previous example, we have substituted words with similar-sounding numbers and acronyms, with nouns and verbs capitalized and other words appearing in lowercase letters.

Or, for instance:

* **I had a dream, where all men were born equal.**
  * *Martin Luther King*

* **1haDwaMwB=**

This appears to be a relatively random password and not so difficult for you to remember as you know the trick of how it was made up. Other tips include substituting numbers for similar-looking letters, abbreviating words that resemble numbers and using emoticons.

* I, i, l, t = 1
* o, O = 0
* s, S = 5, 2
* four, for, fore = 4
* two, to, too = 2
* Are you happy today? = rU:-)2d?

---

You may find these methods difficult to remember. Some theorists posit that these passwords are not as secure as a much simpler method. If you cannot remember your password, it provides no protection to you or your sources. If you find the previous methods too difficult, consider a random assortment of at least four words; longer is better.

These are but basic examples, and you can always create your own method of coding numbers and words. It is advisable that you do so.

---

### Note: Please do not use the examples shown above as your password!

---

## How many passwords do I need?

According to a recent study, the average Internet user has 25 password-protected accounts but uses only 6.5 different passwords across them. In order to manage your passwords effectively, you need to prioritize your security. StoryMaker recommends you use three types of passwords, based on specific needs.

---

## Throw Away Passwords

![](images/4.png)

*Example of a throw away password*

For any site that you simply need a password to register, such as news sites, use a throwaway password. Combine your throwaway password with a throwaway email for added protection. Never use a throwaway password for an account that requires identifying information. This password should not be similar to your higher-security passwords.

---

## Protected Passwords

For any site that requires identifying information, use a protected password. This password should meet the strong password standards mentioned above. Keep any copy of the password secure, especially if the account requires identifying information, such as credit card numbers, or is tied to a personal social media account, such as Facebook or Twitter. You may share some of these passwords with specific individuals such as family or colleagues in case you are unable to access your account personally and need to send verifiable information or close the account suddenly.

---

## Confidential Password

![](images/5.png)

Your confidential password should be known only to you. This password should be used for your primary email account. Any email account that is tied to social media accounts or capable of closing or modifying other accounts must have a confidential password. If you protect only one password, it must be this one. Your primary email account is of utmost importance for your personal safety and that of your sources and colleagues.

Also maintain a confidential password to secure your mobile device. Do not use a PIN, swipe, thumbprint, or facial recognition password. Only a long, typed password should be used for your mobile device.

---

## THINGS TO REMEMBER

* Store any written copy of the password in a secure place.
* Use different passwords for different accounts.
* Use a minimum of 10 characters for your password.
* Better than minimum characters, consider a minimum of four unique words.
* Use a variety of characters, not only letters and numbers.
* Never include obvious personal information.
* Never share your password.
* Change your password every 3-6 months.
* Be sure you transmit your password securely.

---

## QUIZ
(there may be more than one right answer)

#### 1. **How often should you change your password?**

* A. Whenever I feel like it
* B. Yearly
* C. At least every six months
* D. I have never changed my password

#### 2. **Which of the following is an example of a strong password?**

* A. (YourName)2012 eg JohnSmith2012
* B. WeL0vef00dy3sw3do
* C. password
* D. camelsaddlejumpingmonk


#### 3. **Which of the following is an example of proper password maintenance?**

* A. Sharing your password with your significant other
* B. Keeping a single copy of your password in writing in a secure location
* C. Only saving a copy of your password on your computer
* D. Using different passwords for different accounts

---

### ANSWERS

1. C.
2. B. and D.
3. B. and D.